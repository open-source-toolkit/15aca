# 高性能游戏串流解决方案：Sunshine服务端 + Moonlight客户端

## 简介

本仓库提供了一套高效的游戏串流解决方案，包含Sunshine服务端和Moonlight客户端，支持X86、X64架构以及安卓平台。这套方案是目前实现串流游戏或超低延迟的最佳选择，延迟可以控制在20ms以内，并且不挑显卡品牌，无论是AMD、NVIDIA还是Intel的核显都能完美支持。

## 资源内容

- **Sunshine服务端**：用于在服务器端运行游戏，并将游戏画面串流到客户端。
- **Moonlight客户端**：用于接收并显示来自Sunshine服务端的游戏画面，支持X86、X64架构以及安卓平台。

## 特点

- **超低延迟**：延迟可以控制在20ms以内，提供近乎实时的游戏体验。
- **跨平台支持**：支持X86、X64架构以及安卓平台，适用于多种设备。
- **兼容性强**：不挑显卡品牌，无论是AMD、NVIDIA还是Intel的核显都能实现高效串流。
- **开源免费**：基于开源项目，免费使用，社区支持强大。

## 使用方法

1. **安装Sunshine服务端**：
   - 下载并安装Sunshine服务端到你的游戏主机或服务器上。
   - 配置Sunshine服务端，确保其能够正常运行并串流游戏画面。

2. **安装Moonlight客户端**：
   - 根据你的设备平台（X86、X64或安卓），下载并安装对应的Moonlight客户端。
   - 配置Moonlight客户端，连接到Sunshine服务端，开始享受低延迟的游戏串流体验。

## 注意事项

- 确保网络环境稳定，以获得最佳的串流效果。
- 根据设备性能和网络状况，适当调整串流设置以平衡画质和延迟。

## 贡献与支持

欢迎大家贡献代码、提交问题或提供反馈。我们致力于不断优化这套串流解决方案，为大家提供更好的游戏体验。

## 许可证

本项目基于开源许可证发布，具体许可证信息请参阅项目根目录下的LICENSE文件。

---

希望这套解决方案能够帮助你实现低延迟的游戏串流，享受流畅的游戏体验！